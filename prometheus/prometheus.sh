#!bin/bash
helm repo update
helm install --name prometheus stable/prometheus --namespace monitoring -f prometheus/prom-values.yaml
