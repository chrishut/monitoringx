#K8s Monitoring#
The scripts here will install monitoring tools to monitor local kubernetes clusters. Utilizing Prometheus and Grafana these scripts provide open source monitoring in kubernetes clusters with little configuration.

###Installed Components
* Grafana latest
* Prometheus latest helm

###Requirements
* Kubernetes v1.10+
* Helm 2.9.1+
* NGINX (latest helm version)
* Cloud provider storage. Prometheus and Grafana both require persistent storage, the scripts assume vSphere cloud provider integration. If using a different cloud provider update the monitoring-sc.yaml, /prometheus/prom-values, and /grafana/grafanapvc.yaml files with the specific cloud configurations. Please see the *additional resources* section for specific Grafana and Prometheus configuations.

##Installation




##Additional resources
* Prometheus Helm information: https://github.com/helm/charts/tree/master/stable/prometheus
* Grafana information: http://docs.grafana.org/installation/docker/
